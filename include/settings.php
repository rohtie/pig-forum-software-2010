<?php
if(isset($_SESSION['id']) && $_SESSION['rank'] > -1){
$user_r = mysql_query("SELECT avatar,signature FROM user WHERE id='".mysql_real_escape_string($_SESSION['id'])."'");
$user = mysql_fetch_row($user_r);

switch($_GET['s']){
case 'overview':
	echo '<div class="row top">Settings</div>
	<div class="row indicator"><a href="index.php?m=s&amp;a=overview">Settings</a> >></div>
	<div class="row setheader">Account</div>
	<div class="row h settings">
	<form method="post" action="index.php?s=update">
	Old password:<br /><input type="password" name="oldP" /><br />
	New password:<br /><input type="password" name="newP" /><br />
	Repeat:<br /><input type="password" name="repP" /><br />
	<input type="hidden" name="type" value="passwd" />
	<input type="submit" value="Change Password" />
	</form>
	</div>
	<div class="row setheader">Avatar</div>
	<div class="row h settings">
	<div class="quote"><div class="qauthor">Info:</div>
	<div class="qtext">Max 141X143 px - Larger will be resized.<br />Allowed types: jpg,gif,png</div></div>
	Current avatar:<br /><img src="'.$user[0].'" alt="avatar" class="avatar" /><br />
	New avatar:<br />
	<form method="post" action="index.php?s=update" enctype="multipart/form-data">
	<input type="file" name="image" /><input type="submit" value="submit" />
	<input type="hidden" name="type" value="avatar" />
	</form>
	</div>
	<div class="row setheader">Signature</div>
	<div class="row h settings">
	Current signature:<br />'.format_text($user[1]).'
	New signature:<br />
	<form method="post" action="index.php?s=update">
	<textarea name="signature">'.$user[1].'</textarea>
	<input type="hidden" name="type" value="signat" />
	<input type="submit" name="preview" value="preview" /><input type="submit" value="submit" />
	</form>
	</div>
	<div class="row setheader">Stylesheet</div>
	<div class="row h settings">
	Select your style of choice:
	<form method="post" action="index.php?s=update" enctype="multipart/form-data">';
	$directory = dir("custom");
	while ($entry = $directory->read()) {
		if(preg_match("/(\.css$)/i", $entry)){
			$filename[] = $entry;
		}
	}
	$directory->close();
	echo '<select>
	<option value="style.css">Default</option>';
	for($i=0; $i < count($filename); $i++){
		echo '<option value="custom/'.$filename[$i].'">'.$filename[$i].'</option>';
	}
	echo '</select>
	<input type="submit" value="submit" />
	<br /><br />
	Upload custom style:<br />
	<input type="text" /><br />
	<input type="file" name="style_u" />
	<input type="hidden" name="type" value="style" />
	<input type="submit" value="submit" />
	</form>
	</div>';
	break;
case 'update':
	switch($_POST['type']){
	case 'passwd':
		echo '<div class="row top">Settings</div>
		<div class="row indicator"><a href="index.php?s=overview">Settings</a> >> <a href="#">Account</a></div>';
		$res = mysql_query("SELECT password FROM user WHERE id='".mysql_real_escape_string($_SESSION['id'])."'");
		$oldpassword = mysql_fetch_row($res);
		if(sha1($_POST['oldP']) == $oldpassword[0]){
			if($_POST['newP'] == $_POST['repP']){
				mysql_query("UPDATE user SET password='".sha1($_POST['newP'])."' WHERE id='".mysql_real_escape_string($_SESSION['id'])."'");
				rd('index.php?s=overview');
			}else{
				echo errormsg('passwords do not match');				
			}
		}else{
			echo errormsg('wrong password');
		}
		break;
	case 'avatar':
		echo '<div class="row top">Settings</div>
		<div class="row indicator"><a href="index.php?s=overview">Settings</a> >> <a href="#">Avatar</a></div>';
		function getExtension($str) {
			$i = strrpos($str,".");
			if (!$i) { return ""; }
			$l = strlen($str) - $i;
			$ext = substr($str,$i+1,$l);
			return $ext;
		}
	 	$image=$_FILES['image']['name'];
		$errors=0;
	 	if($image){
			//get name, extenstion
	 		$filename = stripslashes($image);
	  		$extension = getExtension($filename);
	 		$extension = strtolower($extension);
			//verify image extension
			if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")){
				echo errormsg('Unwanted file extension');
				$errors=1;
			}else{
				$image_name=time().'.'.$extension;
				$newname="avatar/".$image_name;
				$copied = copy($_FILES['image']['tmp_name'], $newname);
				if (!$copied){
					echo errormsg('Could not copy avatar to fileserver</h1>');
					$errors=1;
				}
				if(!$errors){
					$res = mysql_query("SELECT avatar FROM user WHERE id='".mysql_real_escape_string($_SESSION['id'])."'");
					$oldname = mysql_fetch_row($res);
					//delete old
					if($oldname[0] != 'avatar/default.jpg'){
						unlink($oldname[0]);
					}
					include 'include/resize.php';
					smart_resize_image($newname,141,143,true);
					mysql_query("UPDATE user SET avatar='".$newname."' WHERE id='".mysql_real_escape_string($_SESSION['id'])."'");
					rd('index.php?s=overview');
				}
			}
		}else{
			echo errormsg('You did not upload anyhting');
		}
		break;
	case 'signat':
		if(isset($_POST['preview'])){
			echo '<div class="row top">Settings</div>
			<div class="row indicator"><a href="index.php?s=overview">Settings</a> >> <a href="#">Signature</a> >> <a href="#">preview</a></div>
			<div class="row h settings">
			Preview:<br />'.format_text($_POST['signature']).'
			New signature:<br />
			<form method="post" action="index.php?s=update">
			<textarea name="signature">'.$_POST['signature'].'</textarea>
			<input type="hidden" name="type" value="signat" />
			<input type="submit" name="preview" value="preview" /><input type="submit" value="submit" />
			</form>
			</div>';
		}else{
			echo '<div class="row top">Settings</div>
			<div class="row indicator"><a href="index.php?s=overview">Settings</a> >></div>';
			$query = sprintf("UPDATE user SET signature = '%s' WHERE id = '%s'",
				mysql_real_escape_string($_POST['signature']),
				mysql_real_escape_string($_SESSION['id']));
			mysql_query($query);
			rd('index.php?s=overview');
		}
	break;
	}
break;
}
}else{
	rd('index.php?f');
}
?>
