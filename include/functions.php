<?php
function clean_amp($text){
	$text = str_replace('&','&amp;',$text);
	return $text;
}

function mysql_get($result, $numass=MYSQL_ASSOC) {
	$got = array();
	if(mysql_num_rows($result) == 0){return $got;}
	mysql_data_seek($result, 0);
	while ($row = mysql_fetch_array($result, $numass)) {
		array_push($got, $row);
	} 
	return $got;
}
function mysql_insert($text){
$text =	htmlspecialchars($text);
$text =	mysql_real_escape_string($text);

return $text;
}
function errormsg($text){
	return '<div class="quote"><div class="qauthor">ERROR:</div><div class="qtext"><h1 style="color: #f00;">'.$text.'</h1></div></div>';
}

function rd($destination){
	print '<div class="row h">Redirecting...'.mysql_error().'</div>
	<script type="text/javascript">window.location = "'.$destination.'";</script>';
}

function bbcode($text) {
//[b]
	$text = str_replace('[b]','<strong>',$text);
	$text = str_replace('[/b]','</strong>',$text);
//[i]
	$text = str_replace('[i]','<em>',$text);
	$text = str_replace('[/i]','</em>',$text);
//[u]
	$text = str_replace('[u]','<u>',$text);
	$text = str_replace('[/u]','</u>',$text);
//[img]
	$text = str_replace('[img]','<img src="',$text);
	$text = str_replace('[/img]','" alt="image" />',$text);
//[url]
	$text = preg_replace('/\[url\=(.*?)\](.*?)\[\/url\]/is','<a href="$1">$2</a>',$text);
//[quote]
	$text = preg_replace('/\[quote\=(.*?)\]/is','<div class="quote"><div class="qauthor">$1 wrote:</div><div class="qtext">',$text);
	$text = str_replace('[/quote]','</div></div>',$text);
//[ban]
	$text = str_replace('[ban]','<p><span style="color: #f00;">',$text);
	$text = str_replace('[/ban]','</span></p>',$text);

	return $text;
}

function an2p($text){
$text = explode("\n", $text);
$skip = 0;
for($i=0; $i<count($text); $i++){
    $text[$i] = trim($text[$i]);

	if(strlen($text[$i])){
		$text[$i] = "<p>".$text[$i]."</p>\n";
	}
}

//implode array
$text = implode("", $text);
return $text;
}

function n2p($text){
//Fill array
$text = explode("\n", $text);

for($i=0; $i<count($text); $i++){
    //trim whitespace
    $text[$i] = trim($text[$i]);
	
	//checks wether or not the line is inside a html tag
	if(substr($text[$i], 0, 1) != '<' && substr($text[$i], -1) != '>'){
		//Place <p> tags
		if(strlen($text[$i])){
			$text[$i] = "<p>".$text[$i]."</p>\n";
		}
	}else{
		if(strlen($text[$i])){
			$text[$i] = "\n".$text[$i]."\n";
		}
	}
}

//implode array
$text = implode("", $text);
return $text;
}

function format_text($text){
	$text = htmlspecialchars($text);
	$text = bbcode($text);
	$text = n2p($text);

	return $text;
}
?>
