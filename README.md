# PIG (Phorums Is Good) - 2010 #

Forum software I wrote because there was some features I really wanted in a forum software that didn't exist at the time.

Such as the ability to view forum threads with recent activity that the user has participated in.

I also began working on a feature where users could upload their own custom CSS theme, which is only visible for them.